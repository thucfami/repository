<?php

namespace Chatbot\System\Models;

use Jenssegers\Mongodb\Eloquent\Model;

abstract class BaseModel extends Model
{

    /**
     * format created_at and updated_at is timestamp
     * @var type timestamp
     */
    protected $dateFormat = 'U';

    // updated at and created at is timestamp
    public function freshTimestamp()
    {
        return time();
    }

    // format timestamp
    public function fromDateTime($value)
    {
        if ($value != (int) $value) {
            return strtotime($value);
        }

        return $value;
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes[static::CREATED_AT] = $value;
    }

    public function setUpdatedAtAttribute($value)
    {
        $this->attributes[static::UPDATED_AT] = $value;
    }

    // exchange data
    public function exchange($data = [])
    {
        foreach ($this->attributes as $key => $val) {
            if (isset($data[$key])) {
                $this->exchangeSubAttr($key, $data[$key]);
            }
        }

        // created at
        if ($this->getAttribute(static::CREATED_AT)) {
            $this->setAttribute(static::CREATED_AT, time());
        }

        //uppdated at
        if ($this->getAttribute(static::UPDATED_AT)) {
            $this->setAttribute(static::UPDATED_AT, time());
        }

        return $this;
    }

    // exchane for sub attribute
    public function exchangeSubAttr($key, $dataVal)
    {
        $exVal = $dataVal;
        if (gettype($dataVal) == 'array') {
            $exVal = $this->getAttribute($key);
            foreach ($dataVal as $subkey => $subval) {
                $exVal[$subkey] = $subval;
            }
        }

        if (gettype($dataVal) == 'object') {
            $exVal = $this->getAttribute($key);
            foreach ($dataVal as $subkey => $subval) {
                $exVal->{$subkey} = $subval;
            }
        }

        $this->setAttribute($key, $exVal);
    }

    // enable
    public function enable()
    {
        $this->is_active = true;

        if (isset($this->is_draft)) {
            $this->is_draft = false;
        }

        return $this;
    }

}
